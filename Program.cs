﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace csharp_demo
{
  class Program
  {
    public delegate TResult Func<TResult>();
    public delegate void MethodInvoker();
    static void Main(string[] args)
    {
      // long captured = 1;
      // MethodInvoker x = delegate { Console.WriteLine(captured); captured = 2; };
      // captured = 3;
      // x();
      // Console.WriteLine(captured);
      // captured = 4;
      // x();


      List<MethodInvoker> list = new List<MethodInvoker>();
      for (int index = 0; index < 5; index++)
      {
        int counter = index * 10;
        list.Add(delegate
        {
          Console.WriteLine(counter);
          counter++;
        });
      }

      foreach (MethodInvoker m in list)
      {
        m();
      }
      Console.WriteLine("========");
      list[0]();
      list[0]();
      list[0]();

      list[1]();
    }

    public static IEnumerable<T> Where<T>(IEnumerable<T> source, Predicate<T> predicate)
    {
      if (source == null || predicate == null)
      {
        throw new ArgumentNullException();
      }
      return WhereImpl(source, predicate);

    }
    private static IEnumerable<T> WhereImpl<T>(IEnumerable<T> source, Predicate<T> predicate)
    {
      foreach (T item in source)
      {
        if (predicate(item))
        {
          yield return item;
        }
      }
    }

    static IEnumerable<string> ReadLines(string filename)
    {
      return ReadLines(delegate
      {
        return File.OpenText(filename);
      });
    }
    static IEnumerable<string> ReadLines(Func<TextReader> provider)
    {
      using (TextReader reader = provider())
      {
        string line;
        while ((line = reader.ReadLine()) != null)
        {
          yield return line;
        }
      }
    }

    static IEnumerable<int> CountWithTimeLimit(DateTime limit)
    {
      try
      {
        for (int i = 0; i < 100; i++)
        {
          if (DateTime.Now > limit)
          {
            yield break;
          }
          yield return i;
        }
      }
      finally
      {
        Console.WriteLine("Stopping");
      }
    }

    static readonly string Padding = new string(' ', 30);
    static IEnumerable<int> CreateEnumerable()
    {
      Console.WriteLine("{0}Start of CreateEnumerable()", Padding);
      for (int i = 0; i < 3; i++)
      {
        Console.WriteLine("{0}About to yield {1}", Padding, i);
        yield return i;
        Console.WriteLine("{0}After yield", Padding);
      }
      Console.WriteLine("{0}Yielding final value", Padding);
      yield return -1;
      Console.WriteLine("{0}End of CreateEnumerable()", Padding);
    }
  }

  public class IterationSample : IEnumerable
  {
    object[] values;
    int startingPoint;
    public IterationSample(object[] values, int startingPoint)
    {
      this.values = values;
      this.startingPoint = startingPoint;
    }
    public IEnumerator GetEnumerator()
    {
      // return new IterationSampleIterator(this);
      for (int index = 0; index < this.values.Length; index++)
      {
        yield return values[(index + this.startingPoint) % this.values.Length];
      }
    }

    class IterationSampleIterator : IEnumerator
    {
      IterationSample parent;
      int position;

      internal IterationSampleIterator(IterationSample parent)
      {
        this.parent = parent;
        this.position = -1;
      }
      public object Current
      {
        get
        {
          if (position == -1 || position == parent.values.Length)
          {
            throw new InvalidOperationException();
          }
          int index = position + parent.startingPoint;
          index = index % parent.values.Length;
          return parent.values[index];
        }
      }

      public bool MoveNext()
      {
        if (this.position != parent.values.Length)
        {
          position++;
        }
        return position < parent.values.Length;
      }
      public void Reset()
      {
        this.position = -1;
      }
    }
  }


}